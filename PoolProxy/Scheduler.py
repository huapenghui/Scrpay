from TestProxy import Tester
from ProcyHandler import Getter
from WebApi import app
import time
from multiprocessing import Process
'''
调度模块：调度模块就是将之前定义的获取代理，存储代理，测试代理，模块用多线程的方式进行调度
'''

TESTER_CYCLE = 60
GETTER_CYCLE = 60
TESTER_ENABLE = True
GETTER_ENABLE = True
API_ENABLE = True

class Scheduler():

    def scheduler_tester(self,cycle = TESTER_CYCLE):
        '''
        定时测试代理可用性
        '''
        tester = Tester()
        while True:
            print('测试器开始运行')
            tester.run()
            time.sleep(cycle)

    def schedule_getter(self,cycle=GETTER_CYCLE):
        '''
        定时获取代理
        '''
        getter = Getter()
        getter.run()
        time.sleep(cycle)

    def schedule_api(self):
        '''
        开启API
        :return:
        '''
        app.run()

    def run(self):
        print('代理池开始运行')
        if TESTER_ENABLE:
            tester_process = Process(target = self.scheduler_tester)
            tester_process.start()

        if GETTER_ENABLE:
            getter_process = Process(target = self.schedule_getter)
            getter_process.start()

        if API_ENABLE:
            api_process = Process(target = self.schedule_api)
            api_process.start()

if __name__=='__main__':
    sc = Scheduler()
    sc.run()