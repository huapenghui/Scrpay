import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import learning_curve  # 绘制学习曲线
from sklearn.ensemble import RandomForestRegressor
from sklearn.datasets.samples_generator import make_blobs
from sklearn.svm import SVC  # "Support vector classifier"
from sklearn.datasets.samples_generator import make_circles
'''基于sklearn实现的支持向量机'''

def run_random():
    '''随机生成50个点，分成两堆，并画出散点图'''
    X, y = make_blobs(n_samples=100, centers=2, random_state=0, cluster_std=0.60)
    plt.scatter(X[:, 0], X[:, 1], c=y, s=50, cmap='autumn')
    plt.show()

    '''随便的画几条分割线，哪个好来这？ 看看那个的区分效果更好'''
    xfit = np.linspace(-1, 3.5)  # 生成等间隔数的数据，默认生成50个
    plt.scatter(X[:, 0], X[:, 1], c=y, s=50, cmap='autumn')
    plt.plot([0.6], [2.1], 'x', color='red', markeredgewidth=2, markersize=10)

    for m, b in [(1, 0.65), (0.5, 1.6), (-0.2, 2.9)]:
        plt.plot(xfit, m * xfit + b, '-k')

    plt.xlim(-1, 3.5)
    plt.show()

    '''Support Vector Machines: 最小化 雷区'''
    xfit = np.linspace(-1, 3.5)
    plt.scatter(X[:, 0], X[:, 1], c=y, s=50, cmap='autumn')

    for m, b, d in [(1, 0.65, 0.33), (0.5, 1.6, 0.55), (-0.2, 2.9, 0.2)]:
        yfit = m * xfit + b
        plt.plot(xfit, yfit, '-k')
        plt.fill_between(xfit, yfit - d, yfit + d, edgecolor='none',color='#AAAAAA', alpha=0.4)

    plt.xlim(-1, 3.5)
    plt.show()

def run_svm():
    X, y = make_blobs(n_samples=100, centers=2, random_state=0, cluster_std=0.60)
    plt.scatter(X[:, 0], X[:, 1], c=y, s=50, cmap='autumn')
    '''使用线性核函数,训练一个基本的SVM'''
    model = SVC(kernel='linear')
    model.fit(X, y)

    '''
    这条线就是我们希望得到的决策边界啦
    观察发现有3个点做了特殊的标记，它们恰好都是边界上的点
    它们就是我们的support vectors（支持向量）
    在Scikit-Learn中, 它们存储在这个位置 support_vectors_（一个属性）
    '''
    plot_svc_decision_function(model)

    '''
    观察可以发现，只需要支持向量我们就可以把模型构建出来
    接下来我们尝试一下，用不同多的数据点，看看效果会不会发生变化
    分别使用60个和120个数据点
    只要添加的数据为非支持向量，无论如何添加数据，都不会对决策边界造成任何影响。
    '''
    print(model.support_vectors_)
    plt.show()


def run_rbf():
    '''
    # 引入核函数的SVM
    # 首先我们先用线性的核来看一下在下面这样比较难的数据集上还能分了吗？
    '''
    X, y = make_circles(100, factor=.1, noise=.1)
    clf = SVC(kernel='linear').fit(X, y)
    plt.scatter(X[:, 0], X[:, 1], c=y, s=50, cmap='autumn')
    '''对于这种比较难分的数据集，线性核函数的区分效果会很差，引入非线性核函数'''
    plot_svc_decision_function(clf, plot_support=False)
    plt.show()

    '''
    # 加入径向基函数 rbf，类似于高斯核函数
    # 加入非线性核函数之后，数据的区分效果非常好
    # 使用这种核支持向量机，我们学习一个合适的非线性决策边界。这种核变换策略在机器学习中经常被使用！
    '''
    clf = SVC(kernel='rbf', C=1E6).fit(X, y)
    plt.scatter(X[:, 0], X[:, 1], c=y, s=50, cmap='autumn')
    plot_svc_decision_function(clf)
    plt.scatter(clf.support_vectors_[:, 0], clf.support_vectors_[:, 1], s=300, lw=1, facecolors='none')

    plt.show()

'''对模型的参数C进行调节，观察对数据区分度的影响'''
def run_C():
    '''
    调节SVM参数: Soft Margin问题(软间隔问题，模型的容错能力)
    调节C参数
    当C趋近于无穷大时：意味着分类严格不能有错误,但是模型的泛华能力可能会下降
    当C趋近于很小的时：意味着可以有更大的错误容忍，模型的泛华能力会更好
    '''
    X, y = make_blobs(n_samples=100, centers=2, random_state=0, cluster_std=0.8)

    fig, ax = plt.subplots(1, 2, figsize=(16, 6))
    fig.subplots_adjust(left=0.0625, right=0.95, wspace=0.1)

    '''使用不同大小的C值'''
    for axi, C in zip(ax, [10.0, 0.1]):
        model = SVC(kernel='linear', C=C).fit(X, y)
        axi.scatter(X[:, 0], X[:, 1], c=y, s=50, cmap='autumn')
        plot_svc_decision_function(model, axi)
        axi.scatter(model.support_vectors_[:, 0],
                    model.support_vectors_[:, 1],
                    s=300, lw=1, facecolors='none')
        axi.set_title('C = {0:.1f}'.format(C), size=14)
        print(axi, C)
    plt.show()


'''
# 调节参数gamma，观察对模型复杂度的影响，
# gamma的值越大，模型的复杂程度越高，拟合能力越强，泛华能力越弱；
# gamma的值越小，模型的复杂程度越低，拟合能力越弱，但是泛华能力越强
'''
def run_gamma():
    '''在生成数据时，调节cluster_std 标准差，可以调节数据的离散程度'''
    X, y = make_blobs(n_samples=100, centers=2, random_state=0, cluster_std=1.1)

    fig, ax = plt.subplots(1, 2, figsize=(16, 6))
    fig.subplots_adjust(left=0.0625, right=0.95, wspace=0.1)

    for axi, gamma in zip(ax, [10.0, 0.1]):
        model = SVC(kernel='rbf', gamma=gamma).fit(X, y)
        axi.scatter(X[:, 0], X[:, 1], c=y, s=50, cmap='autumn')
        plot_svc_decision_function(model, axi)
        axi.scatter(model.support_vectors_[:, 0],
                    model.support_vectors_[:, 1],
                    s=300, lw=1, facecolors='none');
        axi.set_title('gamma = {0:.1f}'.format(gamma), size=14)
    plt.show()


'''绘图函数'''
def plot_svc_decision_function(model, ax=None, plot_support=True):
    """Plot the decision function for a 2D SVC"""
    if ax is None:
        ax = plt.gca()
    xlim = ax.get_xlim()
    ylim = ax.get_ylim()

    # create grid to evaluate model
    x = np.linspace(xlim[0], xlim[1], 30)
    y = np.linspace(ylim[0], ylim[1], 30)
    Y, X = np.meshgrid(y, x)
    xy = np.vstack([X.ravel(), Y.ravel()]).T
    P = model.decision_function(xy).reshape(X.shape)

    # plot decision boundary and margins
    ax.contour(X, Y, P, colors='k',
               levels=[-1, 0, 1], alpha=0.5,
               linestyles=['--', '-', '--'])

    # plot support vectors
    if plot_support:
        ax.scatter(model.support_vectors_[:, 0],
                   model.support_vectors_[:, 1],
                   s=300, linewidth=1, facecolors='none');
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)

    plt.show()


if __name__ == '__main__':
    run_gamma()