# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy import Field


class lianjiaItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    title = Field()    # 标题
    zlfs = Field()     # 租赁方式
    hx = Field()       # 户型
    area = Field()     # 面积
    cx = Field()       # 朝向
    price =Field()     # 价格
    flag = Field()     # 标签
    floor = Field()    # 楼层
    dian = Field()     # 用电
    shui = Field()     # 用水
    dt = Field()       # 电梯
    rq = Field()       # 燃气
    cw = Field()       # 车位
    url = Field()      # 链接
    jt = Field()       # 交通
