# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy import Item,Field


class ImageItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    collection = 'images'  #代表mongodb的的集合名称和mysql的表名都为images
    #下面四个字段分别是图片id，链接，标题，缩率图
    id = Field()
    url = Field()
    title = Field()
    thumb = Field()
