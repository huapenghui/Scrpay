# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy import Item,Field

class daxueItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    province = Field()   # 省份
    name = Field()       # 学校名称
    bianhao = Field()    # 学校编号
    zhishujigou = Field() # 直属机构
    diqu = Field()        # 所属城市名称
    jibei = Field()      # 学校级别