
#方式一
# import xml.dom.minidom as xmldom
# import os
# xmlfilepath = os.path.abspath("abc.xml")
# print ("xml文件路径：", xmlfilepath)
#
# # 得到文档对象
# domobj = xmldom.parse(xmlfilepath)
# print("xmldom.parse:", type(domobj))
#
# # 得到元素对象
# elementobj = domobj.documentElement
# print ("domobj.documentElement:", type(elementobj))
#
# #获得子标签
# subElementObj = elementobj.getElementsByTagName("login")
# print ("getElementsByTagName:", type(subElementObj))
# print (len(subElementObj))
#
# # 获得标签属性值
# print (subElementObj[0].getAttribute("username"))
# print (subElementObj[0].getAttribute("passwd"))
#
#
# #区分相同标签名的标签
# subElementObj1 = elementobj.getElementsByTagName("caption")
# for i in range(len(subElementObj1)):
#     print ("subElementObj1[i]:", type(subElementObj1[i]))
#     print (subElementObj1[i].firstChild.data)  #显示标签对之间的数据







#方式二

# import xml.etree.cElementTree as ET
# import os
# import sys
# #遍历xml文件
#
# def traverseXml(element):
#     #print (len(element))
#     if len(element)>0:
#         for child in element:
#             print (child.tag, "----", child.attrib)
#             traverseXml(child)
#     #else:
#         #print (element.tag, "----", element.attrib)
#
#
# if __name__ == "__main__":
#     xmlFilePath = os.path.abspath("hongten.xml")
#     print("xmlfilepath " ,xmlFilePath)
#     try:
#         tree = ET.parse(xmlFilePath)
#         print ("tree type:", type(tree))
#
#         # 获得根节点
#         root = tree.getroot()
#         print('root' , root)
#     except Exception as e:  #捕获除与程序退出sys.exit()相关之外的所有异常
#         print ("parse abc.xml fail!")
#         print (e)
#         sys.exit()
#     print ("root type:", type(root))
#     print (root.tag, "----", root.attrib)
#
#     #遍历root的下一层
#     for child in root:
#         print ("遍历root的下一层", child.tag, "----", child.attrib)
#
#     #使用下标访问
#     print (root[0].text)
#     print (root[1][1][0].text)
#
#     print (20 * "*")
#     #遍历xml文件
#     traverseXml(root)
#     print (20 * "*")
#
#     #根据标签名查找root下的所有标签
#     captionList = root.findall("item")  #在当前指定目录下遍历
#     print (len(captionList))
#     for caption in captionList:
#         print (caption.tag, "----", caption.attrib, "----", caption.text)

#     #修改xml文件，将passwd修改为999999
#     login = root.find("login")
#     passwdValue = login.get("passwd")
#     print ("not modify passwd:", passwdValue)
#     login.set("passwd", "999999")   #修改，若修改text则表示为login.text
#     print ("modify passwd:", login.get("passwd"))


import xml.etree.ElementTree as ET


tree = ET.parse("D:\Sofeware\eclipse-javaee\mygame\database\hongten.xml")

#获取到根节点
root = tree.getroot()
tag = root.tag        #students

#获取根节点的属性
attr = root.attrib   #{}

#获取根节点：students的子节点名称和属性：

for child in root:
    print(child.tag,child.attrib)

for student in root.findall('student'):
    no = student.get('no')
    name = student.find('name').text
    age = student.find('age').text
    print(no, name,age)