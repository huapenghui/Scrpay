#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pymongo
from pandas import DataFrame,Series
import matplotlib.pyplot as plt
import numpy as np


#绘制饼图
def ksh_pie():
    client = pymongo.MongoClient(host='localhost', port=27017)
    db = client['test']  # 指定数据库
    collection = db['WeatherItem']  # 指定集合
    prods = collection.find({'cityname':'beijing'})
    prods.sort('data')
    data = DataFrame(list(prods))
    ##data.sort_values(by='data',ascending=False)  #data列的读入方式不是时间类型，排序并非预想
    data = data.drop(['_id'], axis=1)   #在DF中删除_id字段
    data = data[data['data'] >= '20171101']
    data = data[data['data'] <= '20171231' ]
    data = data['tq']
    data = data.value_counts()
    data1 = data[data>0]
    # print(data.values)
    # print(data.index)
    # print(data)

    labels = data1.index   # 显示在图形上的标签
    sizes = data1.values   # 要在图中显示的数据
    explodes = np.zeros(len(labels))
    explodes[0] = 0.1   #图形中的偏移量，此处设置占比最大的部分偏移0.1

    #解决中文乱码问题
    plt.rcParams['font.sans-serif'] = ['SimHei']
    plt.rcParams['axes.unicode_minus'] = False

    fig1, ax1 = plt.subplots()
    ax1.pie(sizes, explode=explodes ,labels=labels, autopct='%1.1f%%',shadow=True, startangle=90)
    ax1.axis('equal')
    ax1.set(title="北京市天气状况")

    plt.show()

#条形图
def ksh_bar():
    client = pymongo.MongoClient(host='localhost', port=27017)
    db = client['test']  # 指定数据库
    collection = db['WeatherItem']  # 指定集合
    prods = collection.find({'cityname': 'beijing'})
    prods.sort('data')
    data = DataFrame(list(prods))
    data = data.drop(['_id'], axis=1)  # 在DF中删除_id字段
    data = data['tq']
    data = data.value_counts()
    data1 = data[data > 50]

    shanghai = collection.find({'cityname': 'tianjin'})
    shanghai.sort('data')
    sh = DataFrame(list(shanghai))
    sh = sh.drop(['_id'], axis=1)  # 在DF中删除_id字段
    sh = sh['tq']
    sh = sh.value_counts()
    sh1 = sh[sh > 50]


    indbj = np.arange(len(data1.values))
    indsh = np.arange(len(sh1.values))
    width = 0.3

    fig, ax = plt.subplots()
    rects1 = ax.bar(indbj - width/2, data1.values, width,color='SkyBlue',  label='Beijing')
    rects2 = ax.bar(indsh + width/2, sh1.values,   width,color='IndianRed', label='Tianjin')

    # # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_ylabel('次数')
    ax.set_title('北京天津城市天气情况')
    ax.set_xlabel('天气状况')
    ax.set_xticklabels((data1.index))
    ax.legend()

    # 解决中文乱码问题
    plt.rcParams['font.sans-serif'] = ['SimHei']
    plt.rcParams['axes.unicode_minus'] = False

    plt.show()

#折线图
def ksh_plot():
    client = pymongo.MongoClient(host='localhost', port=27017)
    db = client['test']  # 指定数据库
    collection = db['WeatherItem']  # 指定集合
    prods = collection.find({'cityname': 'beijing'})
    prods.sort('data')
    data = DataFrame(list(prods))
    data = data.drop(['_id'], axis=1)  # 在DF中删除_id字段
    data = data[data['data'] >= '20190101']
    date = data['data']
    maxtemp = data['maxtemp']
    mintemp = data['mintemp']
    # # # Data for plotting
    fig, ax = plt.subplots()
    ax.plot(date.values, maxtemp.values,'.-')
    ax.plot(date.values, mintemp.values,'.-')

    # 解决中文乱码问题
    plt.rcParams['font.sans-serif'] = ['SimHei']
    plt.rcParams['axes.unicode_minus'] = False

    ax.set(xlabel='日期', ylabel='温度',title='北京天气')
    ax.grid()  #出现图后面的网格

    plt.show()


#水平折线图
def ksh_barh():
    client = pymongo.MongoClient(host='localhost', port=27017)
    db = client['test']  # 指定数据库
    collection = db['WeatherItem']  # 指定集合
    prods = collection.find({'cityname': 'beijing'})
    prods.sort('data')
    data = DataFrame(list(prods))
    data = data.drop(['_id'], axis=1)  # 在DF中删除_id字段
    data = data['tq']
    data = data.value_counts()
    data1 = data[data > 50]
    # print(data1)

    plt.rcdefaults()
    fig, ax = plt.subplots()

    tq = data1.index
    date = data1.values
    y_pos = np.arange(len(tq))
    performance = 3 + 10 * np.random.rand(len(tq))
    error = np.random.rand(len(tq))

    ax.barh(y_pos, performance, align='center', color='green', ecolor='black')
    ax.set_yticks(y_pos)
    ax.set_yticklabels(tq)
    ax.set_xticklabels(date)
    ax.invert_yaxis()  # labels read top-to-bottom
    ax.set_xlabel('次数')
    ax.set_title('北京天气')

    # 解决中文乱码问题
    plt.rcParams['font.sans-serif'] = ['SimHei']
    plt.rcParams['axes.unicode_minus'] = False
    plt.show()



def daxue_zhanbi():
    client = pymongo.MongoClient(host='localhost', port=27017)
    db = client['test']  # 指定数据库
    collection = db['daxue']  # 指定集合
    daxue = collection.find()
    dx_data = DataFrame(list(daxue))
    dx_data = dx_data.drop(['_id'],axis=1)
    df_jibei = dx_data['jibei']

    df = df_jibei.value_counts()
    print(df)
    labels = df.index   # 显示在图形上的标签
    sizes = df.values   # 要在图中显示的数据

    # 解决中文乱码问题
    plt.rcParams['font.sans-serif'] = ['SimHei']
    plt.rcParams['axes.unicode_minus'] = False

    fig1, ax1 = plt.subplots()
    ax1.pie(sizes, labels=labels, autopct='%1.1f%%', shadow=True, startangle=90)  # 使用pie()方法绘制饼图。
    ax1.axis('equal')
    ax1.set(title="全国高校本科专科占比")  # 设置饼图标题

    plt.show()


def daxue_shuliang():
    client = pymongo.MongoClient(host='localhost', port=27017)
    db = client['test']  # 指定数据库
    collection = db['daxue']  # 指定集合
    daxue = collection.find()
    dx_data = DataFrame(list(daxue))
    dx_data = dx_data.drop(['_id'], axis=1)
    df = dx_data['province']

    df1 = df.value_counts()
    ind = np.arange(len(df1.values))
    width = 0.8

    fig, ax = plt.subplots()
    rects1 = ax.bar(ind,df1.values, width, color='SkyBlue')

    # # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_ylabel('数量')
    ax.set_title('全国高校分布情况')
    ax.set_xlabel('省份')
    ax.set_xticklabels((df1.index))
    ax.legend()

    x = np.arange(len(df1.index))
    # 解决中文乱码问题
    plt.rcParams['font.sans-serif'] = ['SimHei']
    plt.rcParams['axes.unicode_minus'] = False
    plt.yticks(np.arange(0, 180, 10))                  # 设置y轴的刻度范围
    plt.xticks(x, df1.index, rotation=45, fontsize=9)  # 设置x轴上显示的省份个数

    # 在图形上面添加数值，并设置数值的位置
    # for x, y in enumerate(df1.values):
    #     plt.text(x, y + 100, '%s' % round(y,2), ha='left')

    plt.show()



def daxue_shuliang_city():
    client = pymongo.MongoClient(host='localhost', port=27017)
    db = client['test']  # 指定数据库
    collection = db['daxue']  # 指定集合
    daxue = collection.find()
    dx_data = DataFrame(list(daxue))
    dx_data = dx_data.drop(['_id'], axis=1)
    df = dx_data['diqu']

    df1 = df.value_counts()
    df1 = df1[df1.values >= 15]         #因城市数量太多，所以我们只取了大学数量超过15的城市做比对
    ind = np.arange(len(df1.values))
    width = 0.5

    fig, ax = plt.subplots()
    rects1 = ax.bar(ind,df1.values, width, color='SkyBlue')

    # # Add some text for labels, title and custom x-axis tick labels, etc.

    ax.set_title('全国高校分布情况')
    ax.set_xlabel('城市')
    ax.set_ylabel('数量')
    ax.set_xticklabels((df1.index))
    ax.legend()

    x = np.arange(len(df1.index))
    # 解决中文乱码问题
    plt.rcParams['font.sans-serif'] = ['SimHei']
    plt.rcParams['axes.unicode_minus'] = False
    plt.yticks(np.arange(0, 100, 10))                  # 设置y轴的刻度范围
    plt.xticks(x, df1.index, rotation=45, fontsize=9)  # 设置x轴上显示的省份个数

    # 在图形上面添加数值，并设置数值的位置
    # for x, y in enumerate(df1.values):
    #     plt.text(x, y + 100, '%s' % round(y,2), ha='left')

    plt.show()

def daxue_shuliang_prov_jiangsu():
    client = pymongo.MongoClient(host='localhost', port=27017)
    db = client['test']  # 指定数据库
    collection = db['daxue']  # 指定集合
    daxue = collection.find()
    dx_data = DataFrame(list(daxue))
    dx_data = dx_data.drop(['_id'], axis=1)
    dx_data = dx_data[dx_data['province'] == '江苏省']
    dx_data = dx_data.sort_values('diqu')
    df1 = dx_data['diqu']
    df1 = df1.value_counts()

    ind = np.arange(len(df1.values))
    width = 0.5

    fig, ax = plt.subplots()
    rects1 = ax.bar(ind,df1.values, width, color='SkyBlue')

    # # # Add some text for labels, title and custom x-axis tick labels, etc.
    #
    ax.set_title('江苏省高校分布情况')
    ax.set_xlabel('城市')
    ax.set_ylabel('数量')
    ax.set_xticklabels((df1.index))
    ax.legend()
    #
    x = np.arange(len(df1.index))
    # 解决中文乱码问题
    plt.rcParams['font.sans-serif'] = ['SimHei']
    plt.rcParams['axes.unicode_minus'] = False
    plt.yticks(np.arange(0, 60, 10))                  # 设置y轴的刻度范围
    plt.xticks(x, df1.index, rotation=45, fontsize=9)  # 设置x轴上显示的省份个数

    # 在图形上面添加数值，并设置数值的位置
    # for x, y in enumerate(df1.values):
    #     plt.text(x, y + 100, '%s' % round(y,2), ha='left')

    plt.show()


def daxue_shuliang_prov_mean():
    client = pymongo.MongoClient(host='localhost', port=27017)
    db = client['test']  # 指定数据库
    collection = db['daxue']  # 指定集合
    daxue = collection.find()
    dx_data = DataFrame(list(daxue))
    dx_data = dx_data.drop(['_id'], axis=1)

    # print(dx_data)
    dx_prov = dx_data.groupby('province')


    dx_avg_list = []
    for name, group in dx_prov:
        dx_avg_dic = {}
        if name in ('北京市', '天津市', '重庆市', '上海市'):
            continue
        dx_avg_dic['prov'] = name
        dx_avg_dic['avg'] = (group['province'].count()/len(group.groupby('diqu')['name'].count())).round(decimals=2)
        dx_avg_list.append(dx_avg_dic)
    dx_df = DataFrame(list(dx_avg_list))
    df1 = dx_df.sort_values('avg',ascending=False)
    print(df1)
    ind = np.arange(len(df1['avg']))
    width = 0.5

    fig, ax = plt.subplots()
    rects1 = ax.bar(ind, df1['avg'], width, color='SkyBlue')

    # # # Add some text for labels, title and custom x-axis tick labels, etc.
    #
    ax.set_title('全国省份平均高校数量')
    ax.set_xlabel('省份')
    ax.set_ylabel('数量')
    ax.set_xticklabels(df1['prov'])
    ax.legend()

    x = np.arange(len(df1['avg']))
    # 解决中文乱码问题
    plt.rcParams['font.sans-serif'] = ['SimHei']
    plt.rcParams['axes.unicode_minus'] = False
    plt.yticks(np.arange(0, 15, 10))  # 设置y轴的刻度范围
    plt.xticks(x, df1.prov, rotation=45, fontsize=9)  # 设置x轴上显示的省份个数

    plt.show()

if __name__ == '__main__':
    daxue_shuliang_prov_mean()
