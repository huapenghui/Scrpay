# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy import Item,Field


class fundajjItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    #产品编号，日期，单位净值（元），累计净值（元），fqnet，增长值（元），增长率
    product_id = Field()
    date = Field()
    net = Field()
    totalnet = Field()
    fqnet = Field()
    inc = Field()
    rate = Field()
