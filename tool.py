#工具模块类，封装各种公共方法
import pymysql
from pymongo import MongoClient
import logging

def get_connect():
    #获取数据库连接
    dbconn = pymysql.connect(host='localhost', user='root', password='123456', port=3306, db='test')
    loging('获取数据库连接成功','debug')
    return dbconn

def get_cursor(dbconn):
    db_cur = dbconn.cursor()
    loging('获取数据库游标对象成功', 'debug')
    return db_cur


def dyn_insert_sql(tablename,data,dbconn,cursor):
    tablename = tablename
    sql = "select GROUP_CONCAT(COLUMN_name,'') from information_schema.COLUMNS where table_name = %s ORDER BY ordinal_position "
    cursor.execute(sql,tablename)
    tup = cursor.fetchone()
    # 动态构造sql语句
    sql = 'INSERT INTO {table}({keys}) VALUES {values}'.format(table=tablename, keys=tup[0], values=data)
    #使用try-except语句块控制事务的原子性
    try:
        if cursor.execute(sql):
            loging('sucessful,sql is ' + sql,'INFO')
            dbconn.commit()
    except :
        loging('Failed','DEBUG')
        dbconn.rollback()

#将数据存入mongodb数据库
def save_to_mongo(database,document,result):
    client = MongoClient(host='localhost', port=27017)
    db = client[database]   #指定数据库
    collection = db[document]    #指定集合
    if collection.insert_one(result):
        loging('Saved to Mongo Sussessful.', 'DEBUG')


###############################################################
#### 设置日志级别，默认级别为debug
###############################################################
def loging(msg, loglevel='INFO'):  # 默认的日志级别设置为info
    logging.basicConfig(level=logging.DEBUG,format='%(asctime)s - %(filename)s[line:%(lineno)d] - %(levelname)s: %(message)s')
    # logging.basicConfig函数对日志的输出格式及方式做相关配置
    # level=logging.DEBUG,设置日志级别为debug，当日志级别大于或者等于debug时，输出日志
    # 日志级别等级从上到下依次升高的，即：DEBUG < INFO < WARNING < ERROR < CRITICAL，而日志的信息量是依次减少的；
    # debug等级的日志最为详细
    # filename = 'log.txt' 意思为将日志信息输出到文件中，而不是输出到控制台，如果不设置日志文件名称，则日志被会输出到控制台
    if loglevel == 'DEBUG':
        logging.debug(msg)
    if loglevel == 'INFO':
        logging.info(msg)
    if loglevel == 'WARN':
        logging.warning(msg)
    if loglevel == 'ERROR':
        logging.error(msg)
    if loglevel == 'CRITICAL':
        logging.critical(msg)